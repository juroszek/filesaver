﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace FileSaver
{
    public class FileSaver
    {
        private static OutputDirectory _outputDirectory;

        public void Init(OutputDirectory outputDirectory)
        {
            _outputDirectory = outputDirectory;
            ManageFolders(_outputDirectory);
        }

        public void ManageFolders(OutputDirectory outDir)
        {
            if (!Directory.Exists(outDir.OutputPath))
            {
                Directory.CreateDirectory(outDir.OutputPath);
            }

            string[] files = Directory.GetFiles(outDir.OutputPath, "*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                SaveFile(file);
            }

            string[] directories = Directory.GetDirectories(outDir.OutputPath);

            string dailyPattern = @"(\d{4})-(\d{2})-(\d{2})";
            string weeklyPattern = @"(\d{4})-(\d{2}) ([I,II,III,IV,V]+)";
            string monthlyPattern = @"(\d{4})-(\d{2})";

            foreach (string dir in directories)
            {
                string dirName = Path.GetFileName(dir);
                DateTime dirDate = new DateTime();
                if (outDir.Interval == OutputDirectory.FolderInterval.Daily && Regex.Match(dirName, dailyPattern).Success) dirDate = DateTime.ParseExact(dirName, "yyyy-MM-dd", CultureInfo.CurrentCulture);
                if (outDir.Interval == OutputDirectory.FolderInterval.Weekly && Regex.Match(dirName, weeklyPattern).Success) dirDate = FirstDayOfTheWeek(dirName);
                if (outDir.Interval == OutputDirectory.FolderInterval.Monthly && Regex.Match(dirName, monthlyPattern).Success) dirDate = DateTime.ParseExact(dirName, "yyyy-MM", CultureInfo.CurrentCulture, DateTimeStyles.AllowInnerWhite);

                switch (outDir.Interval)
                {
                    case OutputDirectory.FolderInterval.Daily:

                        DateTime dailyBackup = DateTime.Now.AddDays(-(outDir.BackupIntervals));
                        if ((!(Regex.Match(dirName, dailyPattern).Success)) || (dirDate < dailyBackup) && !Directory.EnumerateFileSystemEntries(dir).Any())
                        {
                            Directory.Delete(dir);
                        }
                        break;

                    case OutputDirectory.FolderInterval.Weekly:

                        DateTime weeklyBackup = FirstDayOfTheWeek(DateTime.Now.ToString("yyyy-MM") + " " + ToRoman(DateTime.Now.GetWeekOfMonth())).AddDays(-(7 * (outDir.BackupIntervals - 1)));
                        if (!(Regex.Match(dirName, weeklyPattern).Success) || (dirDate < weeklyBackup) && !Directory.EnumerateFileSystemEntries(dir).Any())
                        {
                            Directory.Delete(dir);
                        }
                        break;

                    case OutputDirectory.FolderInterval.Monthly:

                        DateTime monthlyBackup = DateTime.Now.AddMonths(-(outDir.BackupIntervals));
                        if (!(Regex.Match(dirName, monthlyPattern).Success) || (dirDate < monthlyBackup) && !Directory.EnumerateFileSystemEntries(dir).Any())
                        {
                            Directory.Delete(dir);
                        }
                        break;
                }
            }
        }

        public void SaveFile(string file)
        {
            var outDir = _outputDirectory;
            if (File.Exists(file))
            {
                DateTime fileCreationTime = File.GetLastWriteTime(file);

                string fileName = Path.GetFileName(file);

                switch (outDir.Interval)
                {
                    case OutputDirectory.FolderInterval.Daily:

                        string fileCreationDay = File.GetLastWriteTime(file).ToString("yyyy-MM-dd");
                        DateTime dailyBackup = DateTime.Now.AddDays(-(outDir.BackupIntervals));
                        string dailyDirectory = outDir.OutputPath + "\\" + fileCreationDay + "\\";

                        if (File.Exists(dailyDirectory + fileName) && file != dailyDirectory + fileName) File.Delete(dailyDirectory + fileName);
                        if (!(fileCreationTime < dailyBackup))
                        {
                            if (!Directory.Exists(dailyDirectory))
                            {
                                Directory.CreateDirectory(dailyDirectory);
                                File.Move(file, dailyDirectory + fileName);
                                string[] directories = Directory.GetDirectories(outDir.OutputPath);
                                foreach (var dir in directories)
                                {
                                    if (String.Equals(dailyBackup.AddDays(-1).ToString("yyyy-MM-dd"), Path.GetDirectoryName(dir)))
                                    {
                                        Directory.Delete(dir);
                                    }
                                }
                            }
                            else
                            {
                                File.Move(file, dailyDirectory + fileName);
                            }
                        }
                        else
                        {
                            File.Delete(file);
                        }
                        break;

                    case OutputDirectory.FolderInterval.Weekly:

                        DateTime fileCreationWeek = File.GetLastWriteTime(file);
                        string weeklyFolderName = fileCreationWeek.ToString("MM") + " " + ToRoman(fileCreationWeek.GetWeekOfMonth());
                        DateTime weeklyBackup = FirstDayOfTheWeek(DateTime.Now.ToString("MM") + " " + ToRoman(DateTime.Now.GetWeekOfMonth())).AddDays(-(7 * (outDir.BackupIntervals - 1)));
                        string weeklyBackupFolderName = weeklyBackup.ToString("MM") + " " + ToRoman(weeklyBackup.GetWeekOfMonth());
                        string weeklyDirectory = outDir.OutputPath + "\\" + weeklyFolderName + "\\";

                        if (File.Exists(weeklyDirectory + fileName) && file != weeklyDirectory + fileName) File.Delete(weeklyDirectory + fileName);

                        if (!(fileCreationTime < weeklyBackup))
                        {
                            if (!Directory.Exists(weeklyDirectory))
                            {
                                Directory.CreateDirectory(weeklyDirectory);
                                File.Move(file, weeklyDirectory + fileName);
                                string[] directories = Directory.GetDirectories(outDir.OutputPath);
                                foreach (var dir in directories)
                                {
                                    if (String.Equals(weeklyBackupFolderName, Path.GetDirectoryName(dir)))
                                    {
                                        Directory.Delete(dir);
                                    }
                                }
                            }
                            else
                            {
                                File.Move(file, weeklyDirectory + fileName);
                            }
                        }
                        else
                        {
                            File.Delete(file);
                        }
                        break;

                    case OutputDirectory.FolderInterval.Monthly:

                        string fileCreationMonth = File.GetLastWriteTime(file).ToString("yyyy-MM");
                        DateTime monthlyBackup = DateTime.Now.AddMonths(-(outDir.BackupIntervals));
                        string monthlyDirectory = outDir.OutputPath + "\\" + fileCreationMonth + "\\";

                        if (File.Exists(monthlyDirectory + fileName) && file != monthlyDirectory + fileName) File.Delete(monthlyDirectory + fileName);

                        if (!(fileCreationTime < monthlyBackup))
                        {
                            if (!Directory.Exists(monthlyDirectory))
                            {
                                Directory.CreateDirectory(monthlyDirectory);
                                File.Move(file, monthlyDirectory + fileName);
                                string[] directories = Directory.GetDirectories(outDir.OutputPath);
                                foreach (var dir in directories)
                                {
                                    if (String.Equals(monthlyBackup.AddDays(-1).ToString("yyyy-MM"), Path.GetDirectoryName(dir)))
                                    {
                                        Directory.Delete(dir);
                                    }
                                }
                            }
                            else
                            {
                                File.Move(file, monthlyDirectory + fileName);
                            }
                        }
                        else
                        {
                            File.Delete(file);
                        }
                        break;
                }
            }
        }

        public static string ToRoman(int number)
        {
            string roman = "";
            int n = number;
            while (n > 0)
            {
                if (n >= 5)
                {
                    roman += "V";
                    n -= 5;
                }
                if (n == 4)
                {
                    roman += "IV";
                    n -= 4;
                }
                if (n >= 1 && n < 4)
                {
                    roman += "I";
                    n -= 1;
                }
            }
            return roman;
        }

        public static int ToArabic(string roman)
        {
            int arabic = 0;
            if (roman == "V") arabic += 5;
            if (roman == "IV") arabic += 4;
            if (roman == "III") arabic += 3;
            if (roman == "II") arabic += 2;
            if (roman == "I") arabic += 1;
            return arabic;
        }

        public static DateTime FirstDayOfTheWeek(string folderName)
        {
            DateTime dirDate;
            string dirName = folderName;
            string[] dirNameSplit = dirName.Split(' ');
            string month = dirNameSplit[0];
            int week = ToArabic(dirNameSplit[1]);
            dirDate = DateTime.ParseExact(month, "yyyy-MM", CultureInfo.CurrentCulture);
            GregorianCalendar cal = new GregorianCalendar(GregorianCalendarTypes.Localized);
            int calWeek = cal.GetWeekOfYear(dirDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            int calYear = dirDate.Year;
            DayOfWeek day = DayOfWeek.Monday;
            DateTime startOfYear = new DateTime(calYear, 1, 1);
            int daysToFirstCorrectDay = (((int)day - (int)startOfYear.DayOfWeek) + 7) % 7;
            DateTime FirstDay = startOfYear.AddDays(7 * (calWeek - 2) + daysToFirstCorrectDay);
            if (FirstDay.Month.ToString() != month && week == 1)
            {
                dirDate = FirstDay.AddDays(-FirstDay.Day + 1).AddMonths(1);
            }
            else
            {
                dirDate = FirstDay.AddDays(7 * (week - 1));
            }
            return dirDate;
        }

        public FileSaver()
        {

        }
    }

    static class DateTimeExtensions
    {
        static GregorianCalendar _gc = new GregorianCalendar();
        public static int GetWeekOfMonth(this DateTime time)
        {
            DateTime first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        public static int GetWeekOfYear(this DateTime time)
        {
            return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }
    }
}
