﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace FileSaver
{
    partial class Program
    {
        static void Main(string[] args)
        {
            OutputDirectory outputDirectory;
            FileSaver fileSaver = new FileSaver();

            XmlSerializer Serializer = new XmlSerializer(typeof(OutputDirectory));
            FileStream ReadFileStream = new FileStream(@"instrukcje.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
            outputDirectory = (OutputDirectory)Serializer.Deserialize(ReadFileStream);

            fileSaver.Init(outputDirectory);
            
            string path = @"D:\demoSave.txt";
            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("FileSaverDemo");
                }
            }

            fileSaver.SaveFile(path);
        }
    }
}
