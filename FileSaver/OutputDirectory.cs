﻿using System;
using System.Xml.Serialization;

namespace FileSaver
{
    [Serializable()]
    public class OutputDirectory
    {

        public enum FolderInterval
        {
            Daily,
            Weekly,
            Monthly
        }

        [XmlAttribute]
        public string OutputPath { get; set; }

        [XmlAttribute]
        public FolderInterval Interval { get; set; }

        [XmlAttribute]
        public int BackupIntervals{ get; set; }

        public OutputDirectory()
        {

        }
    }
}
